import pytest
import parser


@pytest.mark.parametrize("audit_record,expected_parsed",
                         [("type=UNKNOWN[1334] msg=audit(1661358743.578:49): prog-id=68 op=LOAD",
                           ({'type': 'UNKNOWN[1334]', 'msg': 'audit(1661358743.578:49)'},
                            {'prog-id': '68', 'op': 'LOAD'}))])
def test_parse_line(audit_record, expected_parsed):
    assert parser.parse_line(audit_record) == expected_parsed


@pytest.mark.parametrize("params_list,split_key_value",
                         [(['prog-id=68', 'op=LOAD'], {'prog-id': '68', 'op': 'LOAD'})])
def test_record_params_kv(params_list, split_key_value):
    assert parser.record_params_kv(params_list) == split_key_value


@pytest.mark.parametrize("audit_message,expected_split",
                         [("audit(1661347659.504:70)", ('70', 'audit(1661347659.504)'))])
def test_split_audit_msg(audit_message, expected_split):
    assert parser.split_audit_msg(audit_message) == expected_split


@pytest.mark.parametrize("line,cleared_line",
                         [("type=UNKNOWN[1334] msg=audit(1661358743.578:49): prog-id=68  op=LOAD\n",
                           "type=UNKNOWN[1334] msg=audit(1661358743.578:49): prog-id=68 op=LOAD")])
def test_sanitize_audit_record(line, cleared_line):
    assert parser.sanitize_audit_record(line) == cleared_line
