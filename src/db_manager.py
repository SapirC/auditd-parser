from pymongo import MongoClient, errors
from sys import exit
import logging


class MongoManager:
    """
    Managing MongoDB,
    connecting or creating a new database
    inserting data
    queries data
    ...

    Attributes
    ----------
        client : MongoClient
            the client connection to the database
        db : MongoClient.Database
            the database

     Methods
    -------

    shutdown_db()

    get_collections()

    get_documents(audit_file_name: str) -> list

    add_audit_file_name(name: str)

    add_new_audit_event(audit_file: str, audit_msg_id: str, type_names: list, fields_types: list)

    """

    def __init__(self, host_ip, port, time_out):
        """
        Initializing the connection of the database
        connecting or creating a new one
        :param host_ip: the ip of the server
        :param port: the port to start the server
        :param time_out: the time to wait for connection until error raised
        """

        try:
            self.__client = MongoClient(host_ip, port,
                                        serverSelectionTimeoutMS=time_out)

        except errors.ServerSelectionTimeoutError as err:
            self.__client = None

            # show pymongo.errors.ServerSelectionTimeoutError
            logging.error(f"pymongo ERROR: {err}")
            exit()

        self.__db = self.__client["audit_data"]

    def shutdown_db(self):
        """ Shutting down the mongodb database """
        self.__client.close()

    def get_collections(self) -> list:
        """ @:return [list] containing all collection names (audit log files) """
        return self.__db.list_collection_names()

    def get_documents(self, audit_file_name: str) -> list:
        """
        @:param [str] audit_file_name collection name of the audit log file
        @:return [list] containing all documents (audit events) of the given collection (audit log file)
        """
        return [doc for doc in self.__db[audit_file_name].find()]

    def add_audit_file_name(self, name: str):
        """
        Adding an audit file as a new collection to the database
        @:param [str] name the name of the file
        """
        col = self.__db[name]

    def add_new_audit_event(self, audit_file: str, audit_msg_id: tuple, type_names: list, fields_types: list):
        """
        Add new audit event, containing records
        :param audit_file: audit file of the records
        :param audit_msg_id: audit message id and timestamp of the records
        :param type_names: type actions of the records
        :param fields_types: fields of the type actions
        :return: None
        """
        col_audit_file = self.__db[audit_file]

        audit_id, timestamp = audit_msg_id

        model_insert = {
            '_id': audit_id,
            'timestamp': timestamp,
        }

        type_exists = False

        # inserting action types into the saved model
        for (type_name, fields) in zip(type_names, fields_types):

            # inserting if not exists or adding missing fields
            find = {
                "_id": audit_id,
                type_name: fields
            }

            # check if not exists
            if col_audit_file.count_documents(find) != 1:
                # inserting action types into the saved model
                model_insert.update({type_name: fields})
            else:
                type_exists = True

        # checking if there's a need to update or add
        if type_exists:
            if len(model_insert) > 1:
                # if at least one type exists, and at least one has been added -> update
                model_insert.pop('_id', None)
                model_insert.pop('timestamp', None)
                col_audit_file.update_one({"_id": audit_id},
                                          {"$set": model_insert})
        else:
            # no type and id exists -> there's a need to add a new one
            col_audit_file.insert_one(model_insert)

        return None

    def display_db(self):
        """ Displays the database collections and documents"""
        for col_name in self.get_collections():
            print(col_name)
            for doc in self.get_documents(col_name):
                print(doc)
