import argparse
import logging

import reader
import db_manager


def arg_parser():
    parser = argparse.ArgumentParser(description='Arguments required for MongoDB database connection '
                                                 '(or create a new one) '
                                                 'in order to save the audit data')
    parser.add_argument("host_ip", help='host ip for the mongodb database connection')
    parser.add_argument("port", help='port for the mongodb database connection (27017)')
    parser.add_argument("timeout", help='time to wait for the creation of the connection until error raised')
    return parser.parse_args()


def main():
    logging_file_name = 'audit-parser.log'

    args = arg_parser()

    logging.basicConfig(filename=logging_file_name, level=logging.DEBUG)

    m_db = db_manager.MongoManager(args.host_ip, int(args.port), int(args.timeout))

    reader.aggregate_audit_files(m_db)

    logging.info(" finished saving/updating ---- displaying database....")

    m_db.display_db()

    print(f"\nNOTICE! logs are at: {logging_file_name}")


if __name__ == '__main__':
    main()

