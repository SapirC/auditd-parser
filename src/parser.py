import logging

from audit_parser_exceptions import ParserError

"""
Used to sanitize every audit record
the sanitation is important because it can cause parse error
the replacement:
old char [key]: new char [value]
"""
CHARS_TRANSLATE_MAP = {
        '  ': ' ',
        '"': '',
        '{ ': '(',
        ' }': ')',
        '\n': ''
    }

# used for making an action happen once
LIMIT = 1


def parse_line(line: str) -> tuple:
    """
    Parses the given audit log record
    @:param line: [str] to parse
    @:return: [tuple] the line parsed to tuple of two dicts,
                to make it easier to save the data
                structure:
                { type , id } , { type fields }
    """

    # separate id, date, type of audit records from fields
    line = sanitize_audit_record(line).split(": ", LIMIT)

    try:
        # splitting all params in the audit record
        line = tuple(record_params_kv(x.split(" ")) for x in line)
    except Exception as error:
        logging.error(f"ParseError: {error}")
        raise ParserError()

    return line


def sanitize_audit_record(line: str) -> str:
    """
    Replaces characters multiple times in the given line
    :param line: [str]
    :return: line after replacement
    """
    for (old, new) in CHARS_TRANSLATE_MAP.items():
        line = line.replace(old, new)
    return line


def record_params_kv(audit_line_key_value_list: list) -> dict:
    """
    Parses the given list into dict
    :param audit_line_key_value_list: [list] the list to parse
    :return: [dict] the result
    """

    # separate each key value pair
    audit_line_key_value_list = (key_value_pair.split('=', LIMIT) for key_value_pair in audit_line_key_value_list)

    # parse list into dict
    return {key: audit_value for key, audit_value in audit_line_key_value_list}


def split_audit_msg(audit_msg_id: str) -> tuple:
    """
    Splitting the id and timestamp from the msg audit id
    :param audit_msg_id: [str] the whole msg audit id record
    :return: [tuple]  id , timestamp
    """
    timestamp, audit_id = audit_msg_id.split(':')

    return audit_id.split(')')[0], f"{timestamp})"
