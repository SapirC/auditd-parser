import logging
from os import listdir

from config import audit_files_dir_path
from parser import parse_line, split_audit_msg
import audit_parser_exceptions


RECORD_HEADER = 0
RECORD_PARAMS = 1


def aggregate_audit_files(m_db):
    # reading every audit file
    for file_name in listdir(audit_files_dir_path):
        with open(audit_files_dir_path + file_name, 'r') as audit_log_file:

            m_db.add_audit_file_name(file_name)
            logging.info(f" Added audit log file {file_name} as a new collection, starting reading audit events...")

            last_audit_id = None
            type_names = []
            fields_types = []
            error_parse = False

            for line in audit_log_file:

                # incase there are other leads that were not caught
                try:
                    parsed = parse_line(line)
                except audit_parser_exceptions.ParserError:
                    logging.error(f" An exception occurred, couldn't parse the line: \n{line}")
                    error_parse = True

                if not error_parse and 'UNKNOWN' not in parsed[RECORD_HEADER]['type']:

                    audit_id, audit_timestamp = split_audit_msg(parsed[RECORD_HEADER]['msg'])

                    if last_audit_id is not None and audit_id != last_audit_id:
                        # add the old audit_id
                        m_db.add_new_audit_event(file_name,
                                                 (last_audit_id, last_audit_timestamp),
                                                 list(type_names),
                                                 list(fields_types))
                        # clear queues
                        type_names.clear()
                        fields_types.clear()

                    # adding the type name and its field to the list of the id
                    type_names.append(parsed[RECORD_HEADER]['type'])
                    fields_types.append(parsed[RECORD_PARAMS])

                    last_audit_id, last_audit_timestamp = audit_id, audit_timestamp
            else:
                # saving the last audit event because the loop of the file has finished
                m_db.add_new_audit_event(file_name,
                                         (last_audit_id, last_audit_timestamp),
                                         list(type_names),
                                         list(fields_types))
                logging.info(" finished saving audit events")
