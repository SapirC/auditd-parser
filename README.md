# auditd-data

![](https://i.imgur.com/l8MaOfN.png)

# setup

## In order to use the program you have to install mongodb

#### in ubuntu 20.04 (ubuntu 22.04 may not work):

[installation described here](https://www.mongodb.com/docs/manual/tutorial/install-mongodb-on-ubuntu/)

`wget -qO - https://www.mongodb.org/static/pgp/server-6.0.asc | sudo apt-key add -`

`echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/6.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-6.0.list`

`sudo apt-get update`

install mongo: `sudo apt-get install -y mongodb-org`

##### make sure the service of mongo is up: `sudo systemctl start mongod`

## install auditd

#### run: `sudo apt-get install auditd`

#### run the auditd service: `sudo service auditd start`

#### make sure the service is running and working: `service auditd status`

#### add any rules from github to `/etc/audit/rules.d/audit.rules`

# How to run:

## get the project:

#### clone to the project: `git clone https://gitlab.com/SapirC/auditd-parser.git`

#### Or

#### download all the files

## install the requirements using pip: `pip install -r requirements.txt`

## Run tests: `pytest tests/`

## Or

## Run the program: `python3 src/main.py localhost 27017 5000`















